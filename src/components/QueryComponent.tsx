// Import Statements:
import { gql, useLazyQuery } from "@apollo/client";
import { Box, Heading, FormControl, FormLabel, Input, Button, Text, Center, Spinner } from "@chakra-ui/react";
import { useFormik } from "formik";
import { useState } from "react";
import React from "react";
import { contributorPolicyID } from "../../gpte-config";

// GraphQL Query Definition:
const UTXO_AT_ADDRESS_WITH_CONTRIBUTOR_TOKEN = gql`
  query GetUTxOAtAddress($address: String!, $tokenPolicyId: Hash28Hex!) {
    utxos(
      where: { _and: [{ address: { _eq: $address } }, { tokens: { asset: { policyId: { _eq: $tokenPolicyId } } } }] }
    ) {
      txHash
    }
  }
`;

// The <QueryComponent /> that is imported to /ppbl-2023-front-end-starter/src/pages/index.tsx
export const QueryComponent = () => {
  // useState hook to store address from user input
  const [queryAddress, setQueryAddress] = useState<string | undefined>(undefined);

  // useFormik hook from formik library
  // Makes it easier to handle forms in React
  const formik = useFormik({
    initialValues: {
      cardanoAddress: "",
    },
    onSubmit: (values) => {
      setQueryAddress(values.cardanoAddress);
    },
  });

  // useLazyQuery hook from @apollo/client library
  // Handles fetching of GraphQL data
  // The name getUTxO can be anything we choose. This is a good name because it descibes what we are doing.
  const [getUTxO, { loading, error, data }] = useLazyQuery(UTXO_AT_ADDRESS_WITH_CONTRIBUTOR_TOKEN);

  // When a button is clicked, set the queryAddress and run the getUTxO query defined above
  const handleClick = () => {
    setQueryAddress(formik.values.cardanoAddress);
    getUTxO({
      variables: {
        address: formik.values.cardanoAddress,
        tokenPolicyId: contributorPolicyID,
      },
    });
  };

  // Boilerplate to handle useLazyQuery loading state.
  if (loading)
    return (
      <Center flexDirection="column">
        <Heading>Loading</Heading>
        <Spinner />
      </Center>
    );

  // Boilerplate to handle useLazyQuery error state.
  if (error)
    return (
      <Center>
        <Heading>Error</Heading>
        <pre>{JSON.stringify(error)}</pre>
      </Center>
    );

    
  // If the query is not loading or in error state, this will be rendered on the page:
  return (
    <Box bg="theme.light" color="theme.dark" p="3">
      <Heading size="md" py="3">
        Check Address For PPBL2023 Token
      </Heading>
      <Text py="3">
        Use this form to check if any address currently holds a PPBL 2023 Contributor Token. Try your address and any others:
      </Text>
      <FormControl p="5" bg="theme.dark" color="theme.light">
        <FormLabel>Enter a Cardano Preprod Address:</FormLabel>
        <Input
          mt="3"
          id="cardanoAddress"
          name="cardanoAddress"
          onChange={formik.handleChange}
          value={formik.values.cardanoAddress}
          placeholder="Preprod Address"
        />
        <Button onClick={handleClick} size="sm" mt="3">
          Check Address
        </Button>
        {queryAddress &&
          data &&
          (data.utxos.length > 0 ? (
            <Box bg="green.400" color="theme.dark" mt="5" p="3" fontSize="sm">
              {queryAddress} currently holds a PPBL2023 Contributor Token.
            </Box>
          ) : (
            <Box bg="red.400" color="theme.dark" mt="5" p="3" fontSize="sm">
              {queryAddress} does not hold a PPBL2023 Contributor Token.
            </Box>
          ))}
      </FormControl>
    </Box>
  );
};
