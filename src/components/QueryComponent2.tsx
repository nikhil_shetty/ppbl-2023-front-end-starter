// Import Statements:
import { contributorPolicyID } from "@/gpte-config";
import { gql, useLazyQuery } from "@apollo/client";
import { Box, Heading, FormControl, FormLabel, Input, Button, Center, Spinner, Divider, Text, Link as CLink } from "@chakra-ui/react";
import { useFormik } from "formik";
import * as React from "react";
import { useState } from "react";

// GraphQL Query Definition:
const TX_FROM_ADDRESS_WITH_POLICYID = gql`
  query TxFromAddressWithPolicyId($address: String!, $tokenPolicyId: Hash28Hex!) {
    transactions(
      where: {
        _and: [
          { inputs: { address: { _eq: $address } } }
          { outputs: { tokens: { asset: { policyId: { _eq: $tokenPolicyId } } } } }
        ]
      }
    ) {
      hash
    }
  }
`;

// The <QueryComponent2 /> that can be imported to /ppbl-2023-front-end-starter/src/pages/index.tsx
export const QueryComponent2 = () => {
  // useState hook to store address from user input
  const [queryAddress, setQueryAddress] = useState<string | undefined>(undefined);

  // useFormik hook from formik library
  // Makes it easier to handle forms in React
  const formik = useFormik({
    initialValues: {
      cardanoAddress: "",
    },
    onSubmit: (values) => {
      setQueryAddress(values.cardanoAddress);
    },
  });

  // useLazyQuery hook from @apollo/client library
  // Handles fetching of GraphQL data
  // The name getUTxO can be anything we choose. This is a good name because it descibes what we are doing.
  const [getTx, { loading, error, data }] = useLazyQuery(TX_FROM_ADDRESS_WITH_POLICYID);

  // When a button is clicked, set the queryAddress and run the getUTxO query defined above
  const handleClick = () => {
    setQueryAddress(formik.values.cardanoAddress);
    getTx({
      variables: {
        address: formik.values.cardanoAddress,
        tokenPolicyId: contributorPolicyID,
      },
    });
  };

  // Boilerplate to handle useLazyQuery loading state.
  if (loading)
    return (
      <Center flexDirection="column">
        <Heading>Loading</Heading>
        <Spinner />
      </Center>
    );

  // Boilerplate to handle useLazyQuery error state.
  if (error)
    return (
      <Center>
        <Heading>Error</Heading>
        <pre>{JSON.stringify(error)}</pre>
      </Center>
    );

  // If the query is not loading or in error state, this will be rendered on the page:
  return (
    <Box bg="theme.light" color="theme.dark" p="3" mt="5">
      <Heading size="md" py="3">
        Check Transaction from Address with Policy ID
      </Heading>
      <Text py="3">
        This form returns the hash of any transaction from the provided address that included a PPBL 2023
        Contributor Token in an output.
      </Text>
      <FormControl bg="theme.dark" color="theme.light" p="5">
        <FormLabel >Enter a Cardano Preprod Address:</FormLabel>
        <Input
          mb="3"
          id="cardanoAddress"
          name="cardanoAddress"
          onChange={formik.handleChange}
          value={formik.values.cardanoAddress}
          placeholder="Preprod Address"
        />
        <Button onClick={handleClick} size="sm">
          Check Address
        </Button>
      </FormControl>

      {data && (
        <>
          <Divider pt="5" />
          <Heading size="md">Query Result</Heading>
          <Box fontSize="sm" fontWeight="bold" p="2" color="theme.light">
            Address: {queryAddress}
          </Box>
          {queryAddress && data &&
            (data.transactions.length > 0 ? (
              <Box bg="green.400" color="theme.dark" mt="5" p="3" fontSize="sm">
                <Text>This address sent a transaction with a PPBL2023 Token as output.</Text>
                <Text>
                  TxHash:{" "}
                  <CLink
                    href={`https://preprod.cardanoscan.io/transaction/${data.transactions[0].hash}`}
                    target="_blank"
                  >
                    {data.transactions[0].hash}
                  </CLink>
                </Text>
              </Box>
            ) : (
              <Box bg="yellow.400" color="theme.dark" mt="5" p="3" fontSize="sm">
                There is no valid transaction from this address.
              </Box>
            ))}
        </>
      )}
    </Box>
  );
};